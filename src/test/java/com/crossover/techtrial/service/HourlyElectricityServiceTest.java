package com.crossover.techtrial.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import com.crossover.techtrial.config.ServiceConfig;
import com.crossover.techtrial.dto.DailyElectricity;
import com.crossover.techtrial.model.HourlyElectricity;
import com.crossover.techtrial.model.Panel;
import com.crossover.techtrial.repository.PanelRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
@Import(ServiceConfig.class)
public class HourlyElectricityServiceTest {
		
	@Autowired
	private HourlyElectricityService hourlyElectricityService;
	
	@Autowired
	private PanelRepository panelRepository;
	
	@Test
	@Sql("/insert_check.sql")
	public void shouldGetHourlyElectricityByPanelId() {
		Panel newPanel = panelRepository.findBySerial("232323");
		assertThat(newPanel).isNotNull();
		
		List<HourlyElectricity> readings = hourlyElectricityService.getAllHourlyElectricityByPanelId(newPanel.getId(), Pageable.unpaged()).getContent();
		assertThat(readings).isNotEmpty();
		assertThat(readings.get(0).getPanel()).isEqualTo(newPanel);		
	}
}
